package b2w.desafio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import b2w.desafio.modelo.Planeta;
import b2w.desafio.repository.PlanetaRepository;

@RestController
@RequestMapping("/api")
public class PlanetaController {

	@Autowired
	private PlanetaRepository repository;


	@RequestMapping(value = "/planeta", method = RequestMethod.GET)
	public List<Planeta> getAllPlaneta(@PathParam("nome") String nome) {
		if(nome != null && !nome.isEmpty())
			return repository.findByNome(nome);
		
		return repository.findAll();
	}

	@RequestMapping(value = "/planeta/{id}", method = RequestMethod.GET)
	public Optional<Planeta> getPlanetaById(@PathVariable("id") String id) {
		return repository.findById(id);
	}

	@RequestMapping(value = "/planeta", method = RequestMethod.POST)
	public Planeta createPlaneta(@Valid @RequestBody Planeta Planeta) {		
		repository.save(Planeta);
		return Planeta;
	}

	@RequestMapping(value = "/planeta/{id}", method = RequestMethod.DELETE)
	  public void deletePlaneta(@PathVariable String id) {
	   repository.deleteById(id);
	  }

}
