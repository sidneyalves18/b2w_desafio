package b2w.desafio.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import b2w.desafio.modelo.Planeta;

public interface PlanetaRepository extends MongoRepository<Planeta, String> {
	
	List<Planeta> findByNome(@Param("nome") String nome);
	
	Optional<Planeta> findById(@Param("id") String id);
}
