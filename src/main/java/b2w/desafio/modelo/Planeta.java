package b2w.desafio.modelo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.util.StringUtils;

public class Planeta {

	@Id
	private String id;

	private String nome;
	private String clima;
	private String terreno;
	@ReadOnlyProperty
	private int aparicaoFilme;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getClima() {
		return clima;
	}

	public void setClima(String clima) {
		this.clima = clima;
	}

	public String getTerreno() {
		return terreno;
	}

	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}

	public int getAparicaoFilme() {
		try {

			String apiUrl = "http://swapi.co/api/planets/";

			HttpClient client = HttpClients.createDefault();

			URIBuilder builder = new URIBuilder(apiUrl);
			builder.addParameter("search", this.nome);

			String urlFull = builder.build().toString();
			HttpGet getMethod = new HttpGet(urlFull);

			HttpResponse httpResponse = client.execute(getMethod);
			int responseCode = httpResponse.getStatusLine().getStatusCode();

			if (responseCode != 200) {
				System.out.println("Falha: HTTP erro codigo: " + responseCode);
				aparicaoFilme = 0;
			}

			String responseBody = EntityUtils.toString(httpResponse.getEntity());
			aparicaoFilme = StringUtils.countOccurrencesOf(responseBody, "/films/");

		} catch (Exception e) {
			System.out.println("Erro ao acessar swapi.co/api - " + e.getMessage());	
			aparicaoFilme = 0;
		}

		return aparicaoFilme;
	}	

}
